package BMIcalculator;

import java.util.Scanner;

/**
 * Created by adam12 on 18.11.17.
 * this program is simple calculator to count BMI weight and write it in console
 * I write this program to try a scanner input but i thing this project can be develop much more.
 * example1 add Gui from JavaFX and then could be fast desktop application to cont BMI
 * example2 add write to file to see the progress for lost weight or other weight.
 * example3 change of language to polish or German i GUI.
 *
 * Based on this simple program we can try more and more improvements try on it.
 */
public class BMIcalculator {
    public static void main(String[] args) {
        // program do wylicznia BMI

        String name;
        double weight;
        double height;
        double bmi;

        Scanner input = new Scanner(System.in);
        System.out.println("");
        System.out.println(" Welcome to BMI Calculator ");
        System.out.println("");
        System.out.println(" What is You name? ");
        name = input.next();
        System.out.println(" Hello " + name);
        System.out.println(" What is You weight in kg?");
        weight = Double.valueOf(input.next().replace(",", "."));
        System.out.println(" What is You height in cm");
        height = Double.valueOf(input.next().replace(",", "."));
        bmi = weight / Math.pow((height / 100), 2);
        System.out.printf("" + name + " your BMI : %.2f \n", bmi);

        if (bmi < 18.5) {
            System.out.print(" You weight is too low, take care you self.");
        } else if (bmi >= 25 && bmi < 29.99) {
            System.out.print(" You are overweight. You must start to lost weight");
        } else if (bmi > 30) {
            System.out.print(" You are obese. You must start to lost weight. " +
                    "Take advance of specialist  ");
        } else {
            System.out.print(" Good You have excellent weight. Good for You.");
        }


    }


}
